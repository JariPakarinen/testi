

Grow with Fab 2018-2019

It is made by Jari Pakarinen FabLab Oulu at Univerisity of Oulu, Finland.
Contact: jari.pakarinen@oulu.fi

Grow with Fab landing page can be found from: 

http://fabdx.academany.org/ Under Fab Lab Oulu

Currently project development blog site is in here: https://jaripakarinen.gitlab.io/testi/

Landing page for the project is at: https://jarifablaboulu.wixsite.com/pcbautomation

Project topic: Automation of PCB throuh hole plating machine using modular IoT-type manipulators to control and automate the old machine.  

Project started as Final project at Fab Academy 2017 from where further development was done with Fab Thesis Alpha year pilot project. Now project will be advanced with Grow with Fab 2018-2019.

For more information visit the www-page or contact the author.

Project is running from November 2018 - July 2019.