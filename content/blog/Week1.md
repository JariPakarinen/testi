---
title: "Week 1"
date: 2018-10-07T11:39:10+02:00
publishdate: 2018-11-19T11:39:10+02:00
image: "/images/blog/wallet.jpg"
tags: ["wallet"]
comments: false
---
# Grow meeting #1 30.10.2018

Gereral outline of the project is:  
3 possible outcomes from Grow with Fab project which are:  
Business  
Education  
Social  
  
Project has three phases:  
1. Design phase  
2. Business perspective (business plan, funding etc.)  
3. Writing of what I did:  
If education: Paper  
If business:  tech. paper  
If social: ..  
  
Meeting dates will be in Google Calendar.  
  
First meeting lecture was given by Georgi of Desing thinking.  
"Desing something that does not exist."  
  
We did some excercises at the meeting and had o homework but Desing ideal wallet.  
During the lesson desing:  

<img src="/images/blog/idealwlecture.jpg">  


First lecture homework is can be seen on this picture.   
We learned last week about the desing and as homework we did a "desing the ideal wallet".  
Desing ended up to a prototype that has different slots for different things like bills, repeipts, cards etc.  
Desing work/flow is shown on the picture below.  
  

<img src="/images/blog/wallet1.jpg">  
<img src="/images/blog/wallet2.jpg">  
<img src="/images/blog/wallet3.jpg">  
<img src="/images/blog/wallet4.jpg">  
<img src="/images/blog/wallet5.jpg">  
<img src="/images/blog/wallet.jpg">  


