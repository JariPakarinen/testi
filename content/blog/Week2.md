---
title: "Week 2"
date: 2018-10-07T11:39:13+02:00
publishdate: 2018-10-07T11:39:13+02:00
image: "/images/blog/ideamap.jpg"
tags: ["interesting"]
comments: false
---
# Grow meeting #2 6.11.2018
Week 3 teaching was about ideating processes that can be used in project/product develpoment.  
Some topics were:  
Design fixation: Getting stuck on old things that have been done and repeating those. Hard to overcome.  
Idea should be: Original and practical (fit the purpose).  
Solutions from nature. Evolution has been working on solutions for millions of years.  
Thematic relations: Use common item on something else, i.e. out of it's usual use envelope.  
Re-orienting things: Quite specific.  
Re-use of clothes pin exercise:  
<img src="/images/blog/clothespin.jpg">   

Homework was to create a mindmap and test one other ideating tool on your own product.   

Tools I used was mindmap and SCAMPER method used to my project.  
After this excercise I made a desicion:  

<b>At this point I desided to concentrate only on the automation modules instead of PCB through hole plating automation for the rest of the Grow with Fab process. I don't see that much potential outside our PCB lab with this automation machine, but modular open source automation unit could have future as a product. This means that I have to shift focus and re-thing the project output and what I will try to accomplish with this focus in mind. And that will be my coal for the rest of the project. </b>
  
Mind map can be seen on the top image. SCAMPER image is here.  

